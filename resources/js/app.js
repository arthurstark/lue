import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import InfiniteLoading from 'vue-infinite-loading'
import ToggleButton from 'vue-js-toggle-button'

import '~/plugins'
import '~/components'

Vue.config.productionTip = false

Vue.use(InfiniteLoading, { /* options */ })

Vue.use(ToggleButton)

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
