<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class PhotoController extends Controller
{
    const PER_PAGE = 10;

    /**
     * Return a list of a images from pastebin
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(Request $request)
    {
        $filterWidth = $request->get('width');
        $filterHeight = $request->get('height');

        $page = $request->get('page');
        $page--;

        $photos = file_get_contents('https://pastebin.com/raw/BmA8B0tY');
        $photos = explode(PHP_EOL, $photos);

        $photos = array_map(function ($photo) {

            $explodedPhoto = explode('/', $photo);

            $height = array_pop($explodedPhoto);
            $width = array_pop($explodedPhoto);
            $id = array_pop($explodedPhoto);

            $photo = trim($photo);
            $width = trim($width);
            $height = trim($height);
            $id = trim($id);

            return [
                'id' => $id,
                'link' => $photo,
                'width' => $width,
                'height' => $height
            ];

        }, $photos);

        $photos = array_filter($photos, function ($photo) use ($filterWidth, $filterHeight) {

            if ($filterWidth && ($filterWidth != $photo['width'])) {
                return false;
            }

            if ($filterHeight && ($filterHeight != $photo['height'])) {
                return false;
            }

            return true;
        });

        $photos = array_values($photos);

        $photos = array_slice($photos, $page * self::PER_PAGE, self::PER_PAGE);

        return response()->json($photos);
    }

    /**
     * Take a link to a image and convert it to a grayscale version
     *
     * @param Request $request
     * @return bool
     */
    public function grayscale(Request $request)
    {
        $imageLink = $request->get('link');

        $image = file_get_contents($imageLink);

        $image = imagecreatefromstring($image);

        imagefilter($image, IMG_FILTER_GRAYSCALE);

        return imagepng($image);
    }
}
