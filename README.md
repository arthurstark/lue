Laravel + Vue Example

Boilerplate https://github.com/cretueusebiu/laravel-vue-spa


## Project goals:

Front End
- Images should be viewable in a grid-like responsive layout
- Gallery should be paginated in the method you best see fit (pages, infinite scrolling, etc.)
- Gallery should include an op)on for filtering images by dimension
- Gallery should include a “toggle grayscale” op)on (appending ?grayscale to any URL will
return a grayscale version of that image)

Back End
- Build a lightweight backend for parsing the .csv file and returning the URLs to the front end
- A RESTful API with an endpoint, or set of endpoints, that the frontend hits to retrieve
images. Endpoint must support:
- retrieving images/filtering images (paginated)
- toggling grayscale

## Installation:
```
composer install

# Create an empty sqlite DB
touch database/database.sqlite

cp .env.example .env

# remove
#        DB_CONNECTION=mysql
#        DB_HOST=127.0.0.1
#        DB_PORT=3306
#        DB_DATABASE=homestead
#        DB_USERNAME=homestead
#        DB_PASSWORD=secret
#
# add  
#       DB_CONNECTION=sqlite
#       DB_DATABASE=ABSOLUTE_PATH_TO_CREATED_SQLITE_DB
#       

php artisan jwt:secret
php artisan key:generate
php artisan migrate

npm install
npm run production
```

## Usage:
```
# Local laravel web server
php artisan serve

# Browse to http://127.0.0.1:8000
```
